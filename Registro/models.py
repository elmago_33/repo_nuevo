# -*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from Contenido.models import *

class Perfil(models.Model):
    nick = models.CharField(max_length = 50, null=True,blank=True)
    nacimiento= models.DateField(blank=True, null=True)
    ciudad = models.ForeignKey(Ciudad,null =True,blank=True)
    foto = models.ImageField(upload_to ='Perfiles',blank=True,null=True)
    
    def __str__(self):
    	return self.nick 

class perfilUsuario(models.Model):
	usuario =models.OneToOneField(User)
	perfil = models.ForeignKey(Perfil)
	def __str__(self):
		return self.perfil.nick