# -*- encoding: utf-8 -*-
from django.shortcuts import render, render_to_response,redirect
from .forms import *
from django.template import RequestContext
from models import *
from django.contrib.auth import authenticate, logout as auth_logout
from django.contrib import auth
from django.core.urlresolvers import reverse
from django.template import RequestContext
import random
import string
from django.core.mail import send_mail,EmailMessage
from django.contrib.auth.decorators import login_required
import datos
from Registro import funcionesRegistro
import json
from django.http import HttpResponseRedirect, HttpResponse
from hashlib import sha1

# def login(request):
#     # context = RequestContext(request, {
#     #     'request': request, 'user': request.user})
#     # return render_to_response('login.html', context_instance=context)
#     if request.method=='POST':
#     	user = request.POST['email']
#     	passw = request.POST['password']
#     	usuario = authenticate(username=user, password=passw)
#     	if usuario is not None:
#     		if usuario.is_active:
#     			auth.login(request,usuario)
#     			request.session['usrLogueado']=user
#     			return redirect("/miembros/")
#     		else:
#     			return redirect(reverse('Registro.views.login'))
#     	else:
#     		form = formLogin()
#     		form2 = formRegistro()
#     		return render(request,'login.html', {"form":form, "formR":form2})
#     else:
#     	form = formLogin()
#     	form2 = formRegistro()
#     	return render(request,'login.html', {"form":form, "formR":form2})


# @login_required(login_url='/')
# def miembros(request):
# 	return render_to_response('miembros.html')


# def logout(request):
# 	auth_logout(request)
# 	return redirect('/')


# def index(request):
# 	form = formLogin()
# 	establecimientos = Establecimiento.objects.all().filter(estado="Activo").order_by("puntaje")[:5]
# 	return render_to_response('index.html',{"Establecimientos":establecimientos, "form":form}, context_instance=RequestContext(request))


# # def index(request):
# # 	#datos.RandomUsers()
# # 	#datos.RandomPlace()
# # 	form = formLogin()
# # 	establecimientos = Establecimiento.objects.all().filter(estado="Activo").order_by("puntaje")[:5]
# # 	return render_to_response('home.html',{"Establecimientos":establecimientos, "form":form},context_instance=RequestContext(request))


# def registro(request):
# 	pass
# # 	if request.method == 'POST':
# # 		form = formRegistro(request.POST)
# # 		if form.is_valid():
# # 			if not User.objects.filter(username=request.POST["email"]):#Verifico que no sea repetido 
# # 			if request.POST['password']== request.POST['confpassword']:
# # 				username = request.POST['email']
# # 				password = request.POST['password']
# # 				email = request.POST['email']
# # 				nombre = request.POST['nombre']
# # 				apellido = request.POST['apellido']
# # 				user = User.objects.create_user(username=username,email= email, password =password,first_name=nombre,last_name=apellido)
# # 				user.is_active = False
# # 				user.save()
# # 				confirmation_code = ''.join(random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for x in range(33))
# # 				p = Perfil(usuario=user, codigo=confirmation_code)
# # 				p.save()
# # 				send_registration_confirmation(user,p)
# # 				return render(request,'registroCorrecto.html',{})
# # 			else:
# # 				return render(request,'registro.html',{'form':form ,'error':'Las passwords ingresadas no coinciden'})
# # 			else:
# # 				return render(request,'registro.html',{'form':form,'error':'El email que ingreso ya esta registrado'})
# # 			else:
# # 				form = formRegistro()	
# # 				return render(request,'registro.html',{'form':form,'error':'Error al ingresar el formulario'})
# # 			else:
# # 				form = formRegistro()	
# # 				return render(request,'registro.html',{'form':form,'error':''})

# #Envio de mail de registracion
# def send_registration_confirmation(user,p):
# 	content = "http://nightadvisor.herokuapp.com/confirmar/" + str(p.codigo) + "/" + user.username
# 	email = EmailMessage('Confirmacion de cuenta ', 'Muchas gracias por registrarse, para confirmar su cuenta, ingrese al siguiente enlace : '+ content,to = [user.username])
# 	email.send()

	
# def confirm(request, confirmation_code,user_name):
# 	try:
# 		user= User.objects.get(username=user_name)
# 		profile = Perfil.objects.get(usuario=user)
# 		if profile.codigo == confirmation_code:
# 			user.is_active = True
# 			user.save()
# 			user.backend='django.contrib.auth.backends.ModelBackend'
# 			form =formLogin()
# 			return render(request,'login.html',{'form':form})
# 	except:
# 		return index(request)
#             #return render(request,'Index.html',{})

# def registroCorrecto(request):
# 	return render(request,'registroCorrecto.html',{})

# # def login(request):
# #     print request.META.get("HTTP_REFERER")
# #     if request.method=='POST':
# #         user = request.POST['email']
# #         passw = request.POST['password']
# #         usuario=authenticate(username=user, password=passw)

# #         if usuario is not None :
# #             if usuario.is_active:
# #                 auth.login(request,usuario)
# #                 request.session['usrLogueado']=user

# #                 #establecimientos = Establecimiento.objects.all().filter(estado="Activo").order_by("puntaje")[:5]
# #                 #return render_to_response('home.html',{"Establecimientos":establecimientos},context_instance=RequestContext(request))
# #                 #return redirect(reverse('Registro.views.index'))
# #                 return index(request)

# #             else:
# #                 return redirect(reverse('Registro.views.login'))
# #         else:
# #             form =formLogin()
# #             return render(request,'home.html',{'form':form})

# #     else:
# #         form =formLogin()
# #         return render(request,'home.html',{'form':form})


# def noActivo(request):
# 	return render(request,'noActivo.html',{})

# # def logout(request):
# #     try:
# #         auth.logout(request)
# #         request.session.flush()
# #         return index(context_instance=RequestContext(request))
# #         #return render_to_response('home.html',{},context_instance=RequestContext(request))
# #     except:
# #         return index(render)
# #         #return render(request,'home.html',{})

# import os

@login_required(login_url="home")
def verPerfil(request,id):
 	usuario = User.objects.get(pk=id)
 	perfil  = Perfil.objects.get(usuario=usuario)
 	contenidos = Contenido.objects.all().filter(estado="Activo").filter(autor=usuario).order_by("fecha")
 	return render_to_response('perfil.html',{'perfil':perfil,'contenidos':contenidos},context_instance=RequestContext(request))




@login_required(login_url="home")
def modificarPerfil(request,id):
 	if request.method=="POST":
 		form = formPerfil(request.POST)
 		if form.is_valid:
 			usuario = User.objects.get(pk=id)
 			pU = perfilUsuario.objects.get(usuario=usuario)	
 			perfil = Perfil.objects.get(id=pU.perfil.id)
 			if request.POST.get("nick")!= "":
 				perfil.nick = request.POST.get("nick")
 			if request.POST.get("nacimiento")!="":
				perfil.nacimiento= request.POST.get("nacimiento")
			

 			if request.POST.get("ciudad") != "":
 				codpos = request.POST["ciudad"]
 				lista = codpos.split(",")
 				ciudSeleccionada=lista[0]       
 				provSeleccionada=lista[1]
 				if not Provincias.objects.filter(nombre = provSeleccionada):
 					provincia = Provincias()
 					provincia.nombre=provSeleccionada
 					provincia.save()
 				else: 
 					provincia = Provincias.objects.get(nombre= provSeleccionada)

 				if not Ciudad.objects.filter(nombre = ciudSeleccionada,provincia_id=provincia):#provincia_id.nombre= provSeleccionada):
 					ciudad= Ciudad()
 					ciudad.nombre=ciudSeleccionada
 					ciudad.provincia_id=provincia
 					ciudad.save()
 				else:
 					ciudad = Ciudad.objects.get(nombre=ciudSeleccionada,provincia_id= provincia)
			
 				perfil.ciudad = ciudad			
 			try:
				perfil.foto= request.FILES['foto']
 			except Exception, e:
 				pass
 			perfil.save()
 			print "guardado"
 			contenidos = Contenido.objects.all().filter(estado="Activo").filter(autor=usuario).order_by("fecha")
 			return render_to_response('perfil.html',{'perfil':perfil,'contenidos':"","usuario":usuario},context_instance=RequestContext(request))
	
 		else:
 			form= formPerfil(instance=perfil)
 			return  render_to_response('modificarPerfil.html',{'form':form},context_instance=RequestContext(request))
 	else:
 		usuario = User.objects.get(pk=id)
 		pU = perfilUsuario.objects.get(usuario=usuario)	
 		perfil = Perfil.objects.get(id=pU.perfil.id)
 		form= formPerfil()
 		return  render_to_response('modificarPerfil.html',{'form':form,'perfil':perfil,"usuario":usuario},context_instance=RequestContext(request))



def selectHistorial(request):
 	reviews = Contenido.objects.filter(autor = usario,estado="Activo",tipo="Review")
 	#mensajes = Mensaje.objects.filter(autor)
 	comentarios = Contenido.objects.filter(autor = usario,estado="Activo",tipo="Comentario")
 	return render(request,"perfil_historial.html",{"coments":comentarios,"reviews":reviews})



def procfile(request):
##La funcion hace lo siguiente: 
	#Si no existe un perfil con el mail del usuario, lo que hace es:
	#ver si el usuario quiere crear un nuevo perfil , o , si se logueo con
	#otra cuenta antes,elegir cual es la cuenta para tener todos el mismo mail 
	if request.method== "GET":
		usuario = request.user
		try:
			pU = perfilUsuario.objects.get(usuario=usuario)
			p = Perfil.objects.get(id=pU.perfil.id)
			return render_to_response('perfil.html',{'perfil':p,'contenidos':"","usuario":usuario},context_instance=RequestContext(request))
		except Exception, e:
			print e
			return render(request,"procfile.html",{})


def procfileCreate(request):
	user = request.user
	p = Perfil(nick=user.username)
	p.save()
	pU = perfilUsuario(usuario=user,perfil=p)
	pU.save()
	return render_to_response('perfil.html',{'perfil':p,'contenidos':"","usuario":user.username},context_instance=RequestContext(request))

def procfileSync(request):
	#Verificar si existe el perfil 
	userRelacion= request.GET.get("user") 
	usuario =[]
	try:
		usuario = User.objects.get(username=userRelacion)
	except Exception, e:
		pass
	if usuario == []:
		return HttpResponse(json.dumps("USUARIOFAIL"))
	else:
		content = "http://nightadvisor.herokuapp.com/confperfil/"  + str(request.user.id) + "/" +str(usuario.id)	
		#content = " http://127.0.0.1:8000/confperfil/" + str(request.user.id) + "/" +str(usuario.id)
 		email = EmailMessage('Confirmacion de sincronización de perfil ', 'El usuario '+" "+ str(request.user.username) +' ha pedido la sincronización de sus perfiles. Para aceptar ingrese al siguiente link '  + content,to = [usuario.email])
 		email.send()
		return HttpResponse(json.dumps("USUARIO"))
		#ver si el usuario tiene perfil, si tiene perfil mandar mail y que 
		#al hacer click en el link, le de permiso a sincronizar 

def confPerfil(request,pide,usPerfil):
	usuarioElegido = User.objects.get(pk=usPerfil)
	perfilElegido = perfilUsuario.objects.get(usuario= usuarioElegido)
	perfil = Perfil.objects.get(pk=perfilElegido.perfil.id)
	user= User.objects.get(pk=pide)
	pU = perfilUsuario(usuario=user,perfil=perfil)
	pU.save()
	usuario = request.user
	try:
		pU = perfilUsuario.objects.get(usuario=usuario)
		p = Perfil.objects.get(id=pU.perfil.id)
		return render_to_response('perfil.html',{'perfil':p,'contenidos':"","usuario":usuario},context_instance=RequestContext(request))
	except Exception, e:
		print e
		return render(request,"procfile.html",{})