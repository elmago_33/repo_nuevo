# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Registro', '0002_remove_perfil_usuario'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='perfil',
            name='codigo',
        ),
    ]
