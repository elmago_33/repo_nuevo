# -*- encoding: utf-8 -*-
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from django.db.models import Count
from Contenido.models import *
from Registro.models import *
from django.contrib.auth.decorators import login_required

# Create your views here.

def index(request):
	establecimientos = Establecimiento.objects.all().filter(estado="Activo").order_by("-puntaje")[:5]	
	return render_to_response('index.html',{"Establecimientos":establecimientos}, context_instance=RequestContext(request))


@login_required(login_url="/accounts/login")
def home(request):
	interacciones = Contenido.objects.all().filter(estado="Activo").order_by("-fecha")[:20]
	tiposEst = TipoEstablecimiento.objects.annotate(cantidad=Count('establecimiento'))
	#tiposEst = TipoEstablecimiento.objects.count()
	return render_to_response('home.html',{ "TiposEst" : tiposEst, "Interacciones": interacciones}, context_instance = RequestContext(request))
