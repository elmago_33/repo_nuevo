"""
Django settings for Reviews project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""



# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_ROOT = os.path.normpath(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ug8wiuo587n9@g%j28)5j#)%)969id*&qx%0i*n!x=*rdaetq5'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

SITE_ID =1

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

ROBOTS_USE_SITEMAP = False

# Application definition

INSTALLED_APPS = (
    'django.contrib.sites',    
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'whoosh',
    'haystack',
    'Core',
    'Registro',
    'Contenido',
    'Mensajes',
    'robots',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.twitter',
    'allauth.socialaccount.providers.google',
)


###allauth

SITE_ID = 1

ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
ACCOUNT_LOGOUT_ON_GET = True
ACCOUNT_AUTHENTICATION_METHOD = 'email'
SOCIALACCOUNT_PROVIDERS = \
    { 'google':
        { 'SCOPE': ['profile', 'email'],
          'AUTH_PARAMS': { 'access_type': 'online' } }}

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # Already defined Django-related contexts here
                "django.contrib.auth.context_processors.auth",
                "django.core.context_processors.debug",
                "django.core.context_processors.i18n",
                "django.core.context_processors.media",
                "django.core.context_processors.static",
                "django.contrib.messages.context_processors.messages",

                # `allauth` needs this from django
                'django.core.context_processors.request',

                # `allauth` specific context processors
                'allauth.account.context_processors.account',
                'allauth.socialaccount.context_processors.socialaccount',
            ],
        },
    },
]

AUTHENTICATION_BACKENDS = (    
    # Needed to login by username in Django admin, regardless of `allauth`
    'django.contrib.auth.backends.ModelBackend',

    # `allauth` specific authentication methods, such as login by e-mail
    'allauth.account.auth_backends.AuthenticationBackend',    
)

LOGIN_REDIRECT_URL = '/miembros/'
SOCIALACCOUNT_PROVIDERS = {
    'google': {
        'SCOPE': ['email'],
        'METHOD': 'oauth2'  # instead of 'oauth2'
    },'facebook':
       {#'SCOPE': ['email', 'public_profile', 'user_friends','publish_stream','publish_actions'],
        'SCOPE':['email','publish_actions','user_friends'],
        'PERMISSIONS':['email','publish_stream','publish_actions'],
        'METHOD': 'oauth2',
        'VERIFIED_EMAIL': False,
        'VERSION': 'v2.3'}

}


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'Reviews.urls'

WSGI_APPLICATION = 'Reviews.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}



# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'es'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

MEDIA_URL  ='/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

STATICFILES_DIRS = (
    os.path.join(os.path.dirname(BASE_DIR), 'static'),
)



STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)





### AUTHENTICATION

ACCOUNT_EMAIL_REQUIRED = True

SOCIAL_AUTH_FACEBOOK_KEY = '1588809968041944'
SOCIAL_AUTH_FACEBOOK_SECRET = '5080c24ecd90d097b2e980d23aab0efc'
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email,publish_stream','publish_actions']
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '906699635590-e8fu13rvjg2a4repmouvp1q4aq9gn2qk.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'AgIyJ2Mw0lC27tHR_RjxzrPG'
SOCIAL_AUTH_TWITTER_KEY = 'UtPcivvTR44zKX5ARfBpcesD3'
SOCIAL_AUTH_TWITTER_SECRET = 'MfNiT02Mv8b2K4MP9F2PKryw1vqPjoEY6YiXlVcsj7iMOLaCgP'


AUTH_PROFILE_MODULE = 'Registro.Perfil'


###EMAIL CONFIGURATION


#EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'nightadvisorteam@gmail.com'
EMAIL_HOST_PASSWORD = 'npilycdppgxjikxe'
EMAIL_PORT = 587



# # motor de busqueda de texto
# HAYSTACK_CONNECTIONS = {
#     'default': {
#         'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
#         'URL': 'http://127.0.0.1:9200/',
#         'INDEX_NAME': 'haystack',
#     },
# }

# motor de busqueda de texto
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(BASE_DIR, 'whoosh_index'),
    },
}


if os.environ.get('HEROKU', False):
 # settings especificas para heroku
    import dj_database_url
    DATABASES['default'] = dj_database_url.config()
    ALLOWED_HOSTS = ['*']
    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
    STATIC_ROOT = 'staticfiles'
    STATIC_URL = '/static/'
    MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

    STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
    )
    from urlparse import urlparse

    es = urlparse(os.environ.get('SEARCHBOX_URL') or 'http://127.0.0.1:9200/')

    port = es.port or 80

    HAYSTACK_CONNECTIONS = {
        'default': {
            'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
            'URL': es.scheme + '://' + es.hostname + ':' + str(port),
            'INDEX_NAME': 'documents',
        },
    }

    if es.username:
        HAYSTACK_CONNECTIONS['default']['KWARGS'] = {"http_auth": es.username + ':' + es.password}
