from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings


urlpatterns = patterns('',
	# Examples:


# url(r'^inicio/','Registro.views.index'),
	
    #url(r'^$','Registro.views.index',name="home"),    
    url(r'^$','Core.views.index',name='index'),
    url(r'^miembros/$','Core.views.home',name='home'),
    (r'^accounts/', include('allauth.urls')),
    #url(r'^registrar/','Registro.views.registro'),
    #url(r'^login/','Registro.views.login'),
    #url(r'^noActivo/','Registro.views.noActivo'),
    #url(r'^logout/','Registro.views.logout'),
    #url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^confirmar/(?P<confirmation_code>\w+)/(?P<user_name>.+)', ('Registro.views.confirm')),
    #url(r'^succes/','Registro.views.registroCorrecto'),    
    url(r'^nuevoEstablecimiento/','Contenido.views.Establecimiento_nuevo'),	

    ###URLS PARA PERFIL
    url(r'^perfil/(?P<id>\w+)/', ('Registro.views.verPerfil')),
    url(r'^procfile/', ('Registro.views.procfile')),
    url(r'^procfileCreate/', ('Registro.views.procfileCreate')),    
    url(r'^modificarPerfil/(?P<id>\d*)',('Registro.views.modificarPerfil')),
    url(r'^procfileSync/', ('Registro.views.procfileSync')),   
    url(r'^confperfil/(?P<pide>\w+)/(?P<usPerfil>.+)', ('Registro.views.confPerfil')),


    url(r'^Establecimientos2/','Contenido.views.Establecimientos2'),
    url(r'DetalleEstablecimiento/(?P<id>\d*)',('Contenido.views.DetalleEstablecimiento')),
    
    url(r'^NuevaReview/(?P<id>\d*)/','Contenido.views.NuevaReview',name='NuevaReview'),
    
    
    url(r'^modificarReview/(?P<id>\d*)',('Contenido.views.modificarReview')),
    url(r'^nuevoComentario/',('Contenido.views.nuevoComentario')),
    
    ###URLS PARA DENUNCIAS ###
    url(r'^denunciar/',('Contenido.views.denunciar')),
    url(r'^denuncias/',('Contenido.views.denuncias')),
    url(r'^moderar/(?P<tipo>\w+)/(?P<id>\d*)$',('Contenido.views.moderar')),
    url(r'eliminarContenido/(?P<id>\d*)$',('Contenido.views.eliminarContenido')),
    url(r'permitirContenido/(?P<id>\d*)$',('Contenido.views.permitirContenido')),
    #url(r'bloquearContenido/',('Contenido.views.bloquarContenido')),


    ###URL PARA ACTUALIZAR CON AJAX LAS NOTIFICACIONES A LOS USUARIOS ###
    url(r'buscarNotificacion/',('Contenido.views.buscarNotificacion')),
    
    ###URLS PARA EL FILTRADO DE CONTENIDOS ####
    url(r'filtrar/',('Contenido.views.filtrar')),
    url(r'filtrarDenuncias/',('Contenido.views.filtrarDenuncias')),

    
    url(r'^Refresh/','Contenido.views.Refresh'),
    url(r'^search/', include('haystack.urls')),    
    url(r'^searchEstablecimiento/', 'Contenido.views.searchEstablecimiento'),    
    
    ### URL Para generar robots.txt
    url(r'^robots\.txt$', include('robots.urls')),


    ###URL Para compartir Redes Sociales
    url(r'postTweet/',('Contenido.views.postTweet')),
    url(r'messageTwitter/',('Contenido.views.sendMessageTwitter')),
    url(r'followers/',('Contenido.views.searchFollowers')),
    url(r'faceWall/',('Contenido.views.wallPost')),
    url(r'faceFriends/',('Contenido.views.faceFriends')),
    url(r'faceMessage/',('Contenido.views.faceMessage')),
    )
    

if settings.DEBUG:
	urlpatterns += patterns('',
		url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
			'document_root': settings.MEDIA_ROOT,
		}),
		url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
			'document_root': settings.STATIC_ROOT,
		}),
)


