# -*- encoding: utf-8 -*-
from allauth.socialaccount.models import SocialAccount,SocialToken,SocialApp
import facebook
import tweepy
from models import*
from operator import itemgetter, attrgetter, methodcaller

def autenticarAppUser(user):
	#Se le pasa el Usuario, y devuelve la instancia de twitter, para no tener que estar en cada 
	#momento autenticando al usuario y a la app
	try:
		social = SocialApp.objects.filter(provider="twitter")
		consumer_key = social[0].client_id
		consumer_secret = social[0].secret
		token= SocialToken.objects.filter(account__user=user, account__provider='twitter')
		socialaccount = SocialAccount.objects.get(user=user, provider="twitter")
		access_token=token[0].token
		access_token_secret= token[0].token_secret
		# Autenticación de La aplicación y de la Cuenta
		auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
		auth.set_access_token(access_token, access_token_secret)
		# Crea la interfaz de usuario
		api = tweepy.API(auth)
		return api ,socialaccount
	except Exception, e:
		return "False","False"

def followers(user):
#==============Buscar los seguidores para poder mandar un mensaje privado ==================================	
	api,socialaccount = autenticarAppUser(user)
	if api=="False":
		return "False"
	else:
		lista=[]
		#Formato de la lista [id,foto,nombre]
		#Me trae La cuenta del proverdor de facebook del usuario 
		for user in tweepy.Cursor(api.followers, id=socialaccount.uid).items():
			nombre = user.screen_name
			imagen= user.profile_image_url_https
			identificador  = user.id
			lista.append([identificador,imagen,nombre,nombre.upper()])
		lista= sorted(lista,key=itemgetter(3))
		return lista



def postTweet(user,tipe,id):
#Postéa en el muro de tweeter 	
	try:
		api,socialaccount= autenticarAppUser(user)
		if api=="False":
			return "False"
		else:
			if tipe =="Principal":
				status = "Encontrá el mejor lugar que se adapta a tu noche en  http://nightadvisor.herokuapp.com"
				photo_path= "/home/augusto/Documentos/enving/Repo/static/img/01.jpg"
			elif tipe == "Establecimiento":
				#establ=get_object_or_404(Establecimiento,pk= id)
				#status = ""+ establ.nombre+ ""
				#photo_path= establ.foto
				pass
		
			api.update_with_media(photo_path, status=status)  
		#Si no se quiere twitear una imagen 
		#api.update_status(status='Hello Python Central!')
			return "Ok"
	except Exception, e:
		raise e
		return "Fail"


def sendMessage(user,user_id):	
#Envía un mensaje privado a la persona que seleccióna, invitandola a que use la página
	try:
		api ,socialaccount= autenticarAppUser(user)
		message= "Encontrá el mejor lugar que se adapta a tu noche en  http://nightadvisor.herokuapp.com" 
		api.send_direct_message(user_id = user_id, text = message)
		return "succes"
	except Exception, e:
		raise e
		return "fail"