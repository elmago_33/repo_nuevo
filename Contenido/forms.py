from django import forms
from .models import *

class FormEstablecimiento(forms.Form):
	nombre = forms.CharField(max_length=50, label="Nombre del Establecimiento ",widget=forms.TextInput())
	tipo = forms.ModelChoiceField(queryset=TipoEstablecimiento.objects.all(),widget=forms.Select())
	
	ciudad= forms.ModelChoiceField(queryset=Ciudad.objects.all().order_by('nombre'),widget=forms.Select())
	
	descripcion= forms.CharField(max_length=500, label="Descripcion ",widget=forms.Textarea(attrs={'required':''}))
	foto = forms.ImageField()

	def clean_image(self):
		#este metodo corrobora que la imagen de los establecimientos tenga como min 1000x600px
		imagen = self.cleaned_data.get['foto']
		if not foto:
			raise forms.ValidationError("Falta la imagen")
		else:
			w, h = get_image_dimensions(imagen)
			if w < 1000:
				raise forms.ValidationError("La imagen tiene %i pixeles de ancho. Debe tener como minimo "%w)

			if h < 600:
				raise forms.ValidationError("La imagen tiene %i pixeles de alto. Debe tener como minimo "%h)


		return imagen                    

class FormEstablecimiento2(forms.ModelForm):
	 class Meta:
	 	model=Establecimiento
	 	fields=["nombre","tipo","descripcion","foto"]

	 	


PUNTAJES= (
(1, ("1")),
(2, ("2")),
(3, ("3")),
(4, ("4")),
(5, ("5")),
	)

class FormNewReview(forms.ModelForm):
	class Meta:
		model= Contenido
		fields= ["titulo","texto", "puntaje"]
		

class FormComentario(forms.ModelForm):
	class Meta:
		model= Contenido
		fields= ["titulo", "texto", "puntaje"]


class LocationForm(forms.Form):	
    class Meta:
        model = Ubicacion
        fields=["ciudad","provincia","pais","longitud","latitud"]

    Lugar = forms.CharField(widget=forms.TextInput(attrs={
                                'class': 'searchTextField'}))
    Ciudad = forms.CharField(widget=forms.HiddenInput(attrs={
                    'name': 'city'}), required=False)
    Provincia = forms.CharField(widget=forms.HiddenInput(attrs={
                    'name': 'state'}), required=False)
    Pais = forms.CharField(widget=forms.HiddenInput(attrs={
                    'name': 'country'}), required=False)
    Latitud = forms.CharField(widget=forms.HiddenInput(attrs={
                    'name': 'latitude'}), required=False)
    Longitud = forms.CharField(widget=forms.HiddenInput(attrs={
                    'name': 'longitude'}), required=False)



class FormDenuncia(forms.ModelForm):
	 class Meta:
	 	model=Denuncias
	 	fields=["razon","categoria"]


	