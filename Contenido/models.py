# -*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


class Establecimiento(models.Model):
	nombre = models.CharField(max_length = 50)
	ciudad = models.ForeignKey('Ciudad')
	puntaje = models.IntegerField()
	tipo = models.ForeignKey('TipoEstablecimiento',default=1)
	descripcion = models.TextField(max_length= 1000)
	foto = models.ImageField(upload_to ='Establecimiento')
	estado=models.CharField(max_length =15,default="Activo")
	creviews=models.IntegerField(default=0)
	direccion = models.CharField(max_length =100,null=True,blank=True,default="No asignada")
	
	def __str__(self):
		return self.nombre
	#Comentado porque me da error a la hora de cargar una nueva review	
	#def save(self):
	#	image = Image.open(self.foto)
	#	size=(350,200)
	#	image = image.resize(size, Image.ANTIALIAS)
	#	image.save(self.photo.path)

	def calcular_puntaje(self):
		promedio = self.contenido_set.all().aggregate(Avg('puntaje'))['puntaje__avg']
		if promedio == None:
			return promedio
		else:
			return int(promedio)


PUNTAJES= (
(1, ("1")),
(2, ("2")),
(3, ("3")),
(4, ("4")),
(5, ("5")),
)	
	
class Contenido(models.Model):
	autor = models.ForeignKey(User)
	titulo = models.CharField(max_length= 50, null=False, blank=False)
	texto = models.TextField(max_length = 500, null=False, blank=False)
	fecha = models.DateTimeField()
	tipo = models.CharField(max_length=50)
	estado = models.CharField(max_length =15,default="Activo")
	puntaje = models.IntegerField(choices=PUNTAJES)
	Establecimiento = models.ForeignKey('Establecimiento',blank=True,null=True)
	relacionado = models.ForeignKey('Contenido',blank = True,null=True)
	def __unicode__(self):
		return 	self.autor.username + self.tipo
	def __str__(self):
		return 	self.autor.username + self.tipo

class ContenidoAntiguo(models.Model):
	autor = models.ForeignKey(User)
	titulo = models.CharField(max_length= 50,default="Contenido")
	texto = models.TextField(max_length = 500)
	fecha = models.DateTimeField()
	tipo = models.CharField(max_length=50)
	estado = models.CharField(max_length =15,default="Activo")
	puntaje = models.IntegerField(choices=PUNTAJES)
	Establecimiento = models.ForeignKey('Establecimiento',blank=True,null=True)
	relacionado = models.ForeignKey('Contenido',blank = True,null=True)
	
	def __str__(self):
		return 	self.autor.username + "contenido " + self.titulo

class TipoEstablecimiento(models.Model):
	tipo = models.CharField(max_length = 50)
	def __str__(self):
		return self.tipo

class Provincias(models.Model):
	nombre = models.CharField(max_length=50)
	def __str__(self):
		return self.nombre

class Ciudad(models.Model):
	nombre= models.CharField(max_length=100)
	provincia_id= models.ForeignKey('Provincias')
	def __str__(self):
		return self.nombre


class Ubicacion(models.Model):
	#fields
	ciudad = models.CharField(max_length=100)
	provincia = models.CharField(max_length=50)
	pais = models.CharField(max_length=50)
	latitud = models.CharField(max_length=15)
	longitud = models.CharField(max_length=15)

class Denuncias(models.Model):
	autor = models.ForeignKey(User)
	tipo = models.CharField(max_length=25)
	fecha = models.DateTimeField()
	id_denunciado= models.IntegerField()
	razon = models.CharField(max_length=100)
	categoria= models.ForeignKey('CategoriaDenuncias',blank=True,null=True)
	estado = models.CharField(max_length=25,default="Activo")
	def __str__(self):
		return self.autor.username 

class CategoriaDenuncias(models.Model):
	nombre= models.CharField(max_length=25)
	def __str__(self):
		return self.nombre
