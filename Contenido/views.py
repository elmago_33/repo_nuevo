# -*- encoding: utf-8 -*-

from django.shortcuts import render
from forms import *
from django.template import RequestContext
from django.shortcuts import render, render_to_response,redirect,get_object_or_404
from models import *
from Mensajes.models import *
from Registro.models import * 
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from datetime import datetime
from django.http import HttpResponseRedirect, HttpResponse
#Para el paginador
import funciones
import tweet
import json
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from operator import itemgetter, attrgetter, methodcaller

from haystack.query import SearchQuerySet
from haystack.views import SearchView

from allauth.socialaccount.models import SocialAccount,SocialToken,SocialApp
import facebook
import tweepy

def buscar(id):
	establ=get_object_or_404(Establecimiento,pk= id)
	return establ


@login_required(login_url="/accounts/login/")
def Establecimiento_nuevo(request):
	if request.method=="POST":
		form=FormEstablecimiento2(request.POST,request.FILES)
		
		nombre = request.POST["nombre"]
		ti = request.POST["tipo"]
		tipo = get_object_or_404(TipoEstablecimiento,pk=ti)
		codpos = request.POST["ciudad"]
		lista = codpos.split(",")
		ciudSeleccionada=lista[0]		
		provSeleccionada=lista[1]
		if not Provincias.objects.filter(nombre = provSeleccionada):
			provincia = Provincias()
			provincia.nombre=provSeleccionada
			provincia.save()
		else: 
			provincia = Provincias.objects.get(nombre= provSeleccionada)

		if not Ciudad.objects.filter(nombre = ciudSeleccionada,provincia_id=provincia):#provincia_id.nombre= provSeleccionada):
			ciudad= Ciudad()
			ciudad.nombre=ciudSeleccionada
			
			ciudad.provincia_id=provincia
			ciudad.save()
		else:
			ciudad = Ciudad.objects.get(nombre=ciudSeleccionada,provincia_id= provincia)
		puntaje = 1 
		descripcion = request.POST["descripcion"]
		foto = request.FILES["foto"]
		E = Establecimiento(nombre=nombre,ciudad = ciudad,puntaje = puntaje,tipo = tipo,descripcion = descripcion,foto = foto)
		E.save()
		return redirect('/Establecimientos2/')
	else:
		form= FormEstablecimiento2()
		return render(request,'EstablecimientoNuevo.html',{'form':form})




def Establecimientos2(request):                            
	return render_to_response('Establecimientos2.html',context_instance=RequestContext(request))


#@login_required(login_url="index")
def DetalleEstablecimiento(request,id):
	establ = buscar(id)
	form= FormComentario()
	#comentarios = Contenido.objects.filter(tipo="Review",Establecimiento=establ,estado="Activo" )
	#return render_to_response('DetalleEstablecimiento.html',{'form':form,'Establecimiento':establ,'Reviews':comentarios},context_instance=RequestContext(request))
	fDenuncia= FormDenuncia()
	fReview= FormNewReview()
	lista=[]
	perfilUsr, perfil = None, None
	reviews= Contenido.objects.filter(tipo="Review",Establecimiento=establ ,estado="Activo")
	for r in reviews:
		#c = Contenido.objects.filter(tipo="Comentario",relacionado=r.id,estado="Activo")
		try:
			perfilUsr  = perfilUsuario.objects.get(usuario_id=r.autor.id)

		except perfilUsuario.DoesNotExist:
			perfilUsr = None

		if perfilUsr != None:
			try:
				perfil = Perfil.objects.get(pk=perfilUsr.id)

			except Perfil.DoesNotExist:
				perfil = None	

		if perfil != None:
			lista.append((r,perfil.foto))
		else:
			lista.append((r,'Perfiles/no-user.png'))

	return render_to_response('DetalleEstablecimiento.html',{"lista":lista,"Establecimiento":establ,"fReview":fReview,'form':form,"fDenuncia":fDenuncia},context_instance=RequestContext(request))




@login_required(login_url="/accounts/login/")
def NuevaReview(request,id):
	if request.method=="POST":
		autor= request.user
		texto = request.POST["texto"]
		fecha= datetime.now()
		titulo = request.POST["titulo"]
		tipo = "Review"
		activo= "Activo"
		puntaje = request.POST["puntaje"]
		establecimiento = buscar(id)
		try:
			R = Contenido(autor=autor,titulo = titulo,texto = texto,fecha= fecha,tipo=tipo,estado=activo,puntaje=puntaje,Establecimiento=establecimiento)
			R.save()
		except Exception, e:
			raise e
			print e
			establ = buscar(id)
			form = FormNewReview()
			return	render(request,'NuevaReview.html',{'form':form,'Establecimiento':establ})
		
		establ = buscar(id)

		prom = funciones.promedioReviews(establ)	
		establ.puntaje = prom
		establ.save()
		return redirect('Contenido.views.DetalleEstablecimiento', id=establ.id)
		#return DetalleEstablecimiento(request,establ.id)
		
		#return redirect("DetalleEstablecimiento/"+str(establ.id))
		#comentarios = Contenido.objects.filter(tipo="Review",Establecimiento=establ )
		#return render_to_response('DetalleEstablecimiento.html',{'Establecimiento':establ,'Reviews':comentarios},context_instance=RequestContext(request))
	


@login_required(login_url="/accounts/login/")
def modificarReview(request,id):
	if request.method =='POST':
		antigua =Contenido.objects.get(pk=id)
		RA =ContenidoAntiguo()
		#Generar la version de la Review
		RA.autor= request.user
		RA.titulo= antigua.titulo
		RA.texto=antigua.texto
		RA.fecha= datetime.now()
		RA.estado="Versionado"
		RA.tipo="Review"
		RA.puntaje = antigua.puntaje
		RA.Establecimiento =  antigua.Establecimiento
		#Generar la nueva Review
		autor= request.user
		texto = request.POST["texto"]
		fecha= datetime.now()
		titulo = request.POST["titulo"]
		tipo = "Review"
		puntaje = request.POST["puntaje"]
		establecimiento = antigua.Establecimiento
		try:
			#Salvamos nueva version
			R = Contenido(autor=autor,titulo = titulo,texto = texto,fecha= fecha,tipo=tipo,estado="Activo",puntaje=puntaje,Establecimiento=establecimiento)
			R.save()

			RA.relacionado=R
			#Salvamos la version antigua
			RA.save()
			antigua.delete()
		except Exception, e:
			raise e
			print e
			review = Contenido.objects.get(pk=id)
			form = FormNewReview(instance= review)
			return	render(request,'modificarReview.html',{'form':form,'Establecimiento':review.Establecimiento})
	
		establ = buscar(R.Establecimiento.id)
		##################  Modificar  Promedio ##########################
		prom = funciones.promedioReviews(establ)	
		establ.puntaje = prom
		establ.save()

		comentarios = Contenido.objects.filter(tipo="Review",Establecimiento=establ )
		return render_to_response('DetalleEstablecimiento.html',{'Establecimiento':establ,'Reviews':comentarios},context_instance=RequestContext(request))
	else:
		review = Contenido.objects.get(pk=id)
		form = FormNewReview(instance =review)
		return	render(request,'modificarReview.html',{'form':form,'review':review})


@login_required(login_url="/accounts/login/")
def eliminarContenido(request,id):
	denuncias = Denuncias.objects.filter(id_denunciado=id)
	bandera=0
	for denuncia in denuncias:
		denuncia.estado	="Eliminada"
		denuncia.save()
	contenido = Contenido.objects.get(pk=id)
	autor = contenido.autor
	if contenido.tipo =="Review":
		######Cambiar el puntaje del establecimiento######
		bandera=1
		establ = buscar(contenido.Establecimiento.id)
		prom = funciones.promedioReviews(establ)	
		establ.puntaje = prom
		establ.save()
		relacionados = Contenido.objects.filter(relacionado=contenido)
		for r in relacionados:
			r.estado="Eliminado"
			r.save()
		notificar = funciones.notificacion(request.user,autor,"ReviewDenunciadoEliminado") 
	else:
		notificar = funciones.notificacion(request.user,autor,"ComentarioDenunciadoEliminado") 
	contenido.estado="Eliminado"
	contenido.save()
	if bandera==1:
		#establ = buscar(establ.id)
		prom = funciones.promedioReviews(establ)	
		establ.puntaje = prom
		establ.save()

	direccion =str(request.META.get("HTTP_REFERER"))
	if "/moderar/" in direccion:
		
		lDenuncias = Denuncias.objects.all().order_by("fecha")
		return render_to_response('denuncias.html',{'denuncias': lDenuncias},context_instance=RequestContext(request))
	else:
		return redirect("/")


@login_required(login_url="/accounts/login/")
def nuevoComentario(request):
	if request.method =='POST':
		try:
		
			autor = request.user
			texto = request.POST['texto']
			titulo=" "
			tipo = 'Comentario'
			fecha = datetime.now()
			estado="Activo"
			puntaje = 0
			id_relacionado= request.POST["review_id"]
			relacionado= Contenido.objects.get(pk=id_relacionado)

			Cont = Contenido(autor = autor,texto=texto,titulo=titulo,relacionado= relacionado,tipo=tipo,fecha=fecha,estado=estado,puntaje=puntaje)
			Cont.save()
			establ = relacionado.Establecimiento
		
			lista=[]
			reviews= Contenido.objects.filter(tipo="Review",Establecimiento=establ ,estado="Activo")
			for r in reviews:
				c = Contenido.objects.filter(tipo="Comentario",relacionado=r.id, estado="Activo")
				lista.append((r,c))
			notificar = funciones.notificacion(autor,relacionado.autor,"Comentario")
			return render_to_response('DetalleEstablecimiento.html',{"lista":lista,"Establecimiento":establ},context_instance=RequestContext(request))

		#return render_to_response('DetalleEstablecimiento.html',{'Establecimiento':establ,'Reviews':reviews},context_instance=RequestContext(request))
		except Exception, e:
			raise e
	else:	
		pass



@login_required(login_url="/accounts/login/")
def denunciar(request):
	tipo = request.POST["tipo"]
	nu= request.POST["id_denuncia"]
	print "numero " + nu
	num = int(nu,10)
	estab=""
	if tipo == "Review":
		contenido = Contenido.objects.get(pk=num)
		tipo = "Review"	
		estab= contenido.Establecimiento.id	
	elif tipo == "Comentario":
		contenido = Contenido.objects.get(pk=num)
		tipo = "Comentario"
	elif tipo == "Usuario":
		contenido = User.objects.get(pk=num)
		tipo= "Usuario"
	else:
		pass
	autor = request.user
	cataux= request.POST["categoria"]
	categoria=CategoriaDenuncias.objects.get(id=cataux)
	fecha = datetime.now()
	id_denunciado = num
	razon = request.POST['razon']
	denuncia = Denuncias(autor= autor, fecha=fecha,id_denunciado=id_denunciado,razon=razon,tipo=tipo,categoria=categoria)
	denuncia.save()
	#return HttpResponseRedirect(reverse( reverse('DetalleEstablecimiento', args=(),
                    #kwargs={'id': estab})))
	return render_to_response('Establecimientos2.html',context_instance=RequestContext(request))


#@login_required(login_url="index")
def denuncias(request):
	
	lDenuncias = Denuncias.objects.all().order_by("fecha")
	print lDenuncias
	return render_to_response('denuncias.html',{'denuncias': lDenuncias},context_instance=RequestContext(request))


@login_required(login_url="/accounts/login/")
def moderar(request,tipo,id):
 	if request.method =="POST":
 		pass
	else:
		if tipo=="Review":
 			review= Contenido.objects.get(pk=id)
 			
			return render_to_response("moderar.html",{"review":review,"comentarios":" ","usuario":" ","vista":"Review/","id":id},context_instance=RequestContext(request))	
		elif tipo == "Comentario":
			comentario = Contenido.objects.get(pk=id)

			return render_to_response("moderar.html",{"review":" ","comentarios":comentario,"usuario":" ","vista":"Comentario/","id":id},context_instance=RequestContext(request))



@login_required(login_url="/accounts/login/")
def permitirContenido(request,id):
	contenido = Denuncias.objects.filter(id_denunciado=id)
	id_denuncia=id
	for c in contenido :
		c.estado ="Permitida"
		c.save()
		print c.estado 
	cont= Contenido.objects.get(pk= id_denuncia)
	if cont.tipo== "Review":
		review = Contenido.objects.get(pk=id)
		return render_to_response("moderar.html",{"review":review,"comentarios":" ","usuario":" ","vista":"Review/","id":id},context_instance=RequestContext(request))
	elif tipo == "Comentario":
		comentario = Contenido.objects.get(pk=id)
		return render_to_response("moderar.html",{"review":" ","comentarios":comentario,"usuario":" ","vista":"Comentario/","id":id},context_instance=RequestContext(request))


@login_required(login_url="/accounts/login/")
def bloquearContenido(request,id):
	#1 Cambiar el estado a bloqueado
	try:
		denuncia = Denuncias.objects.filter(id_denunciado=id)
		for d in denuncia :
			d.estado="Bloqueado"
			d.save()
		return ""		

	except Exception, e:
		raise e
	


#2 enviar mensaje al usuario, diciendo que debe modificar contenido

	
# 3 Cuando el estado modifica el contenido , el estado pasa a activo nuevamente

def buscarNotificacion(request):
	id = request.GET.get("id")
	remitente = User.objects.filter(id=id)
	try:
		notificaciones = Mensaje.objects.filter(remitente=remitente,mostrado="F")	
		print notificaciones
		lista =[]
		for n in notificaciones:
			n.mostrado="V"
			n.save()
			lista.append([n.texto])
			return HttpResponse(json.dumps(lista))
	except Exception, e:
		print e
		lista = []
		return HttpResponse(json.dumps(lista))



def Refresh(request):
	establ = buscar(id)
	form= FormComentario()
	#comentarios = Contenido.objects.filter(tipo="Review",Establecimiento=establ,estado="Activo" )
	#return render_to_response('DetalleEstablecimiento.html',{'form':form,'Establecimiento':establ,'Reviews':comentarios},context_instance=RequestContext(request))
	fDenuncia= FormDenuncia()
	lista=[]
	reviews= Contenido.objects.filter(tipo="Review",Establecimiento=establ ,estado="Activo")
	for r in reviews:
		c = Contenido.objects.filter(tipo="Comentario",relacionado=r.id,estado="Activo")
		lista.append((r,c))
	return render(request,"ComentariosReviews.html",{"lista",lista})



#Vista para la busqueda con Haystack (Pasarlo luego a otro archivo .py)
def searchEstablecimiento(request):
	q = request.GET.get("datos")
	r2 = SearchQuerySet().autocomplete(nombre=q)#Busca establecimientos por nombre
	r1 = SearchQuerySet().autocomplete(ciudad=q)#Busca establecimientos por ciudad
	r3 = SearchQuerySet().autocomplete(texto=q)#Busca contenido por el texto
	r4 = SearchQuerySet().autocomplete(usuario=q)#Busca Usuarios por el nombre
	r = r1 | r2 |r3 |r4
	return render(request,'resultados.html',{'resultados':r})


#Filtrado de la table establecimientos segun el tipo , para el template Establecimientos2
def filtrar(request):
	tipo = request.GET.get("tipo")
	print tipo
	lista = funciones.filtrarEstablecimientos(tipo)
	#pasa por este lugar tambien
	return render(request,'tablaEstablecimiento.html',{"Establecimientos":lista})


def filtrarDenuncias(request):
	tipo = request.GET.get("tipo")
	if tipo =="Todos":
		denuncias= Denuncias.objects.all()
	else:	
		denuncias= Denuncias.objects.filter(tipo=tipo)
	return render(request,'tablaDenuncias.html',{"denuncias":denuncias})


#=============================Compartir Contenido Face-Twitter-ETC==================================


#=================FACEBOOK================================================================================
def wallPost(request):		
	try:
		socialaccount = SocialAccount.objects.get(user=request.user, provider="facebook")
	except Exception, e:
		return HttpResponse(json.dumps("False"))
	

	token= SocialToken.objects.filter(account__user=request.user, account__provider='facebook')
	graph = facebook.GraphAPI(token[0])


  	message = "Compartí con tus amigos los mejores boliches y bares de la zona"
  	attachment =  {
     	'name': 'NightAdvisor',
     	'link': 'http://nightadvisor.herokuapp.com/',
     	'caption': 'Compartí con tus amigos los mejores boliches y bares de la zona',
     	'description': 'Sitio de reviews de bares y boliches',
     	'picture': 'http://nightadvisor.herokuapp.com/static/img/01.jpg'
 }
 	try:
 		graph.put_wall_post(message=message,attachment= attachment)
 		return HttpResponse(json.dumps("ok"))
 		print "OK"
 	except Exception, e:
 		raise e
		return HttpResponse(json.dumps("fail"))


def faceMessage(request):
	id_usuario= request.GET.get("id")

	try:
		socialaccount = SocialAccount.objects.get(user=request.user, provider="facebook")
	except Exception, e:
		return HttpResponse(json.dumps("False"))
	

	token= SocialToken.objects.filter(account__user=request.user, account__provider='facebook')
	graph = facebook.GraphAPI(token[0])
	linkeado = graph.get_object(id=id_usuario)
	print linkeado
  	message = "Compartí con tus amigos los mejores boliches y bares de la zona"
  	attachment =  {
     	'name': 'NightAdvisor',
     	'link': 'http://nightadvisor.herokuapp.com/',
     	'caption': 'Compartí con tus amigos los mejores boliches y bares de la zona',
     	'description': 'Sitio de reviews de bares y boliches',
     	'picture': 'http://nightadvisor.herokuapp.com/static/img/01.jpg'
 }
 	try:
 		graph.put_wall_post(message=message, attachment=attachment,profile_id=linkeado)
 		#graph.put_wall_post(message=message,attachment= attachment)
 		return HttpResponse(json.dumps("ok"))
 		print "OK"
 	except Exception, e:
 		raise e
		return HttpResponse(json.dumps("fail"))


def faceFriends(request):
	lista= []
	try:
		socialaccount = SocialAccount.objects.get(user=request.user, provider="facebook")
	except Exception, e:
		print e
		return HttpResponse(json.dumps("False"))
	token= SocialToken.objects.filter(account__user=request.user, account__provider='facebook')
	graph = facebook.GraphAPI(token[0])
 	print "Graph",graph
 	##Hasta aca, va perfecto
 	try:
 		friends = graph.get_connections('me','invitable_friends')
 		for friend in friends["data"]:
 			print friend
 			lista.append([friend["id"],friend["picture"]["data"]["url"],friend["name"]])
 		lista= sorted(lista,key=itemgetter(2))
 		return render(request,'friends.html',{"followers":lista})
 		
 	except Exception, e:
 		raise e
 		print e
		return HttpResponse(json.dumps("fail"))
 	


def searchFollowers(request):
	usuario =request.user 
	lista = tweet.followers(usuario)
	if lista == "False":
		return HttpResponse(json.dumps("False"))	
	else:
		return render(request,'followers.html',{"followers":lista})

def sendMessageTwitter(request):
	id_e = request.GET.get("Id")
	result=tweet.sendMessage(request.user,id_e)
	return HttpResponse(json.dumps(result))	

def postTweet(request):
#Se llama a la funcion desde AJAX,se pasa dentro del data un parametro llamado "Tipo", el cual , si
#es principal, se hace un tweet compartiendo la pagina, si no , se pasan en tipo="Establecimiento",y "id"
#con el id del establecimiento, para compartir elo establecimiento
	id_e = request.GET.get("Id")
	tipo = request.GET.get("Tipo")
	result = tweet.postTweet(request.user,tipo,id_e)
	return HttpResponse(json.dumps(result))	
 