from django.contrib import admin
from Contenido.models import *

admin.site.register(Establecimiento)
admin.site.register(Contenido)
admin.site.register(Ciudad)
admin.site.register(Provincias)
admin.site.register(TipoEstablecimiento)
admin.site.register(CategoriaDenuncias)
admin.site.register(Denuncias)