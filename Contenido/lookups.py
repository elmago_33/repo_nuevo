from __future__ import unicode_literals

from selectable.base import ModelLookup
from selectable.registry import registry

from .models import Ciudad

class CiudadLookup(ModelLookup):
	model=Ciudad
	search_fields=('nombre_icontains',)

registry.register(CiudadLookup)