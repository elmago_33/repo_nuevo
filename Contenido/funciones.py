# -*- encoding: utf-8 -*-
from Contenido.models import *
from Mensajes.models import *
from datetime import datetime

def promedioReviews(establ):
	reviews = Contenido.objects.all().filter(Establecimiento=establ).filter(estado="Activo")
	cantidad= 0
	suma= 0
	for r in reviews:
		cantidad =cantidad+1
		suma =suma + r.puntaje

	if cantidad == 0 :
		return 0
	else:
		return suma//cantidad


def notificacion(autor,remitente,tipo):
	if tipo =="Comentario":
		texto = autor.username +"ha comentado tu review"	
	if tipo =="ComentarioComentario":
		texto = autor.nombre +" hizo un comentario en tu comentario: " 
	elif tipo =="ReviewDenunciadoEliminado":
		texto = "El moderardor" + autor.username +"ha eliminado tu review"
	elif tipo =="ComentarioDenunciadoEliminado":
		texto = "El moderador " + autor.username +"ha eliminado tu comentario"
	fecha = datetime.now()
	mensaje = Mensaje(autor=autor,remitente=remitente,texto=texto,fecha=fecha,estado="Activo",tipo="Notificacion",revisado="F",mostrado="F")
	mensaje.save()
	return True	



def filtrarEstablecimientos(campo):
	if campo =="MejoresPuntuados":
		return Establecimiento.objects.all().order_by('-puntaje')
	
	elif campo == "Bar":
		tipo = TipoEstablecimiento.objects.get(tipo=campo)
		return Establecimiento.objects.filter(tipo=tipo)

	elif campo == "Restaurant":
		tipo = TipoEstablecimiento.objects.get(tipo=campo)
		return Establecimiento.objects.filter(tipo=tipo)

	elif campo == "Boliche":
		tipo = TipoEstablecimiento.objects.get(tipo=campo)
		return Establecimiento.objects.filter(tipo=tipo)

	elif campo == "Pub":
		tipo = TipoEstablecimiento.objects.get(tipo=campo)
		return Establecimiento.objects.filter(tipo=tipo)
			
	elif campo== "Todos":
		return  Establecimiento.objects.all()


	else:
		return []
	
def filtrarDenuncias(campo):
	return  Denuncias.objects.filter(tipo=campo)

def post_to_facebook(request):
  try:
    fb_user = FacebookUser.objects.get(user = request.user)
    # GraphAPI is the main class from facebook_sdp.py
    graph = GraphAPI(fb_user.access_token)
    attachment = {}
    message = 'test message'
    caption = 'test caption'
    attachment['caption'] = caption
    attachment['name'] = 'test name'
    attachment['link'] = 'link_to_picture'
    attachment['description'] = 'test description'
    graph.put_wall_post(message, attachment)
    return 0
  except:
    logging.debug('Facebook post failed')
