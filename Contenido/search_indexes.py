# -*- encoding: utf-8 -*-
from haystack import indexes
from Contenido.models import *
from Registro.models import *

class ContenidoIndex(indexes.SearchIndex, indexes.Indexable):
     text = indexes.CharField(document=True, use_template=False)
     autor = indexes.EdgeNgramField(model_attr='autor')
     texto = indexes.EdgeNgramField(model_attr='texto')
      	
     def get_model(self)	:
         return Contenido

     def index_queryset(self, using=None):
         return self.get_model().objects.filter(estado="Activo")


class EstablecimientoIndex(indexes.SearchIndex,indexes.Indexable):
	text = indexes.CharField(document=True, use_template=False)
	descripcion = indexes.EdgeNgramField(model_attr="descripcion")
	nombre = indexes.EdgeNgramField(model_attr='nombre')
	ciudad = indexes.EdgeNgramField(model_attr="ciudad")
	tipo = indexes.EdgeNgramField(model_attr="tipo")
	
	def get_model(self):
		return Establecimiento
	
	def index_queryset(self, using=None):
		return self.get_model().objects.filter(estado="Activo")


class UsuarioIndex(indexes.SearchIndex,indexes.Indexable):
	text = indexes.CharField(document=True, use_template=False)
	usuario= indexes.EdgeNgramField(model_attr="username")
	
	def get_model(self):
		return User
	
	def index_queryset(self, using=None):
		return self.get_model().objects.filter(is_active=True)