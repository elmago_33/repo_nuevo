# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Mensaje',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('texto', models.TextField(max_length=500)),
                ('fecha', models.DateTimeField()),
                ('estado', models.CharField(default=b'Activo', max_length=25)),
                ('revisado', models.CharField(default=b'F', max_length=1)),
                ('mostrado', models.CharField(default=b'F', max_length=1)),
                ('tipo', models.CharField(default=b'Mensaje', max_length=25)),
                ('autor', models.ForeignKey(related_name='Autor', to=settings.AUTH_USER_MODEL)),
                ('remitente', models.ForeignKey(related_name='Remitente', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
