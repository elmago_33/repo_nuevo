from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Mensaje(models.Model):
	autor = models.ForeignKey(User,related_name='Autor')
	remitente = models.ForeignKey(User, related_name='Remitente')
	texto = models.TextField(max_length=500)
	fecha = models.DateTimeField()
	estado = models.CharField(max_length=25,default="Activo")
	revisado= models.CharField(max_length=1,default="F")
	mostrado = models.CharField(max_length=1,default="F")
	tipo  = models.CharField(max_length=25,default="Mensaje")
	def __str__(self):
		return self.autor.username

